(ns techascent.core
  (:require [clojure.string :as s]
            [tech.ml.dataset :as ds]
            [tech.ml.dataset.column :as ds-col]
            [tech.v2.datatype :as dtype]))

(defn foo
  "I don't do a whole lot."
  [x]
  (println x "Hello, World!"))
